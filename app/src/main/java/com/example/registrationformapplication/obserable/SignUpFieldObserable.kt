package com.example.registrationformapplication.obserable

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.library.baseAdapters.BR

class SignUpFieldObserable:BaseObservable() {

    @get:Bindable
    var fname:String=""
    set(value) {
        field=value
        notifyPropertyChanged(BR.fname)
    }

    @get:Bindable
    var lname:String=""
    set(value) {
        field=value
        notifyPropertyChanged(BR.lname)
    }

    @get:Bindable
    var mnumber:String=""
    set(value) {
        field=value
        notifyPropertyChanged(BR.mnumber)
    }


}