@file:Suppress("UNCHECKED_CAST")

package com.example.registrationformapplication.factory

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.registrationformapplication.repository.SignUpSharedPrefRepository
import com.example.registrationformapplication.viewModal.SignUpSharedPrefRepositoryViewModel

class SignUpSharedPrefRepositoryViewModalFactory(private val sharedPrefRepo: SignUpSharedPrefRepository, private val context: Context):ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(SignUpSharedPrefRepositoryViewModel::class.java)){
            return SignUpSharedPrefRepositoryViewModel(sharedPrefRepo,context) as T
        }
        throw IllegalArgumentException(" Known Class ")
    }
}