package com.example.registrationformapplication.viewModal

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.registrationformapplication.obserable.FieldObserable

class FormActivityviewModal :ViewModel() {
    private var _firstname:MutableLiveData<String> = MutableLiveData()
    private var _lastname:MutableLiveData<String> = MutableLiveData()
    private var _mobilenumber:MutableLiveData<String> = MutableLiveData()
    private var _altnumber:MutableLiveData<String> = MutableLiveData()
    private var _dob:MutableLiveData<String> = MutableLiveData()
    private var _email:MutableLiveData<String> = MutableLiveData()
    private var _pannumber:MutableLiveData<String> = MutableLiveData()
    private var _adharcard:MutableLiveData<String> = MutableLiveData()
    private var _address:MutableLiveData<String> = MutableLiveData()
    private var _gender:MutableLiveData<String> = MutableLiveData()
    private var _hobbies:MutableLiveData<ArrayList<String>> = MutableLiveData()
    private var errorMessage : MutableLiveData<String> = MutableLiveData()

    val baseObserable:FieldObserable= FieldObserable()

    init {
        _firstname.value=""
        _lastname.value=""
        _mobilenumber.value=""
        _altnumber.value=""
        _dob.value=""
        _email.value=""
        _pannumber.value=""
        _adharcard.value=""
        _address.value=""
        errorMessage.value=""

    }

    var msg : LiveData<String> = errorMessage

    fun datavalidation(gender:String,arrayList:ArrayList<String>):Boolean {
        return if (baseObserable.fname.isNotEmpty()&&baseObserable.lname.isNotEmpty()&&baseObserable.mobilenumber.isNotEmpty()
            &&baseObserable.altnumber.isNotEmpty()&&baseObserable.dob.isNotEmpty()&&baseObserable.email.isNotEmpty()
            &&baseObserable.pannumber.isNotEmpty()&&baseObserable.adharcard.isNotEmpty()&&baseObserable.address.isNotEmpty()
            &&gender.isNotEmpty()&&arrayList.isNotEmpty()){
            errorMessage.value=" All Fields are Fill "
            true

        }else{
            errorMessage.value=" All Fields is Empty "
            false
        }

    }


}